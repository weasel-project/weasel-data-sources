""" Collection of data sources for the WEASEL project. """

from weasel_data_sources.mapping.mapping import TechnologyBuilder

__version__ = "2.3.0"

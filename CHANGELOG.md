# Changelog

<!--next-version-placeholder-->

## v2.3.0 (2021-03-05)
### :sparkles:
* :sparkles: Create SVN fetcher or CVS-agnostic repository fetchers through source mappings and technology handlers  ([`3bc5765`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/3bc57653b687b9ac063184a0782f60a27398f03d))
* :sparkles: Add first implementation of a SVN fetcher  ([`11c019e`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/11c019e7274d576d9de0af41f53275a6ef388bdb))

### Other
* Merge branch 'svn-crawler' into 'master' ([`b59bd04`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/b59bd0458611c5fd2b5c63cf1f8e496e3d61d6ef))
* :construction_worker: Fix previous commit  ([`251f84a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/251f84a6004d099ab8c058549fdebe62800c953e))
* :construction_worker: Add missing dependency in CI run  ([`9582cf3`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/9582cf3e1e0ff1b31e3d000b989cb6c629f35a9c))
* :white_check_mark: Restructure existing unittests to also cover SVNFetcher + bugfixes  ([`519cc6b`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/519cc6bcaa29b48d3a6f95f818bb07e2adc678d9))
* :memo: Add SVNFetcher class to api docs  ([`c5af8a5`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/c5af8a536d441769ce0844fc25f2cd0236fe4cfd))
* Ignore some bandit warnings  ([`2483df2`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/2483df2bbc79f90f409925a0343bfb8142fa48c6))

## v2.2.4 (2021-03-01)
### Other
* Merge branch 'version-sorting' into 'master' ([`fcbd3d9`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/fcbd3d9026827dd29db50b352a8542e957801b50))
* Sort release metadata by version string  ([`1af8bc3`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/1af8bc3b016e158d228d2186e8fdbece8c945a97))

## v2.2.3 (2021-02-19)
### Other
* Merge branch 'remove-unused-tech' into 'master' ([`1d6a2f4`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/1d6a2f44c2a8ca333a69861d6f123821fc43b05b))
* Remove PHPFusion as it has no valid release sources  ([`8a4d54a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/8a4d54a7cfc9ae21b64e96d7352dff53979467fc))

## v2.2.2 (2021-02-17)
### Other
* Merge branch 'authors' into 'master' ([`7368043`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/736804353f9f28d341c4e59018e68bf60c9ee4a3))
* Add authors  ([`c943b0f`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/c943b0f306162e6c41e6a936afdb62c136f90192))

## v2.2.1 (2021-02-16)
### Other
* Merge branch 'docs' into 'master' ([`f44de93`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/f44de93655b7d3dc049e91113e6c42c64a053dbe))
* :green_heart: Fix problem with running the tests on Python 3.6  ([`388e012`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/388e012877f7b90b7f067212676bd89ff45235db))
* :memo: :construction_worker: Update CI settings for documentation builds  ([`9ad62ba`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/9ad62ba9250e2c9d547bfe6a2874bab86c409209))
* :memo: Add Basics to the documentation  ([`f5a7b33`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/f5a7b33a15765ffffb78c8b7815f79acb9e4c959))
* :memo: Add example for technology mappings  ([`26b01f6`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/26b01f6aa0f4c71e24adb4f8dbfd9c0353d1e39b))
* :memo: Make the example crawler reusable  ([`8c22c37`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/8c22c3708d6cbc8713d0f1f544c203ffda9c17c3))
* :memo: Add example of file retrieval  ([`0d8c596`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/0d8c5964a5e286545e7f69be852b4127206f3315))
* :rotating_light: Fix problem with safety warnings for build dependencies  ([`0cc7200`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/0cc720013a41c4b37aaf8f2e3e9dbf91cb8e4869))
* :memo: Add basic API documentation  ([`e6c530b`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/e6c530bc912dd7df2040503a67db2c281c23d39f))

## v2.2.0 (2021-02-14)
### :sparkles:
* :sparkles: First draft of vulnerability crawlers  ([`477ffd6`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/477ffd6a4abdb1c572895a250983096a1e8f2644))

### Other
* Merge branch 'cve-search' into 'master' ([`17c450a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/17c450a6aa3d5c6af4f8a437067a4167b354f63a))
* Add CWEs  ([`ebafa0a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/ebafa0a6888a42732a493377ad1b7f3d82c42817))
* Remove instance attributes to pass linting  ([`52aa4e5`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/52aa4e50ddc9b43705de3e04d3a0c43ed17d3945))
* Reformat file  ([`151fc1a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/151fc1a42d69ec41b22af9b0ef426bc8db740a4e))

## v2.1.0 (2021-02-09)
### :sparkles:
* :sparkles: Add ability to substitute contents of version tags  ([`342c9fb`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/342c9fb00e1e5edf9db5a4466a7552c0ebbd8973))
* :sparkles: Implemented foundations of data source mappings and fetcher factories  ([`b60afb4`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/b60afb45b44abcc6d0610205af5b81993e8d2db0))

### :bug:
* :bug: Fix linting problem  ([`213d1f9`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/213d1f9ea1bb7e4d428d999242501538b02fddce))
* :bug: Remove duplicate entry of FancyBox  ([`57fa411`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/57fa411c63cbfb4c2fc7db396aca2bfd2f79adbc))
* :bug: Exclude symbolic links from the file list, as they may point outside of the repository  ([`fcf7d18`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/fcf7d18bcb31f7e80d14fbe99eff1d051b5a8492))
* :bug: Fix failure on initialization if the local repository is not following a branch  ([`4085538`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/40855388f20e709a5e73fe48e6ba1745aa42ab4b))

### Other
* Merge branch 'source-mappings' into 'master' ([`74753b8`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/74753b8536432abeceddf8b894c0abc51f27b44c))
* :recycle: Refactor the safe name method to a public method  ([`86a2850`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/86a2850c6e9aab075d86ff9ae2a0a5163d86848d))

## v2.0.1 (2021-02-04)
### Other
* Merge branch 'python-support' into 'master' ([`573e377`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/573e3770deca4f2f6e70a5a1510513d8553495b6))
* :wrench: Also test with Python 3.6 and 3.7  ([`9d68bb6`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/9d68bb6f6c0542f8aac2fc7e46456d5f1fb0fe1d))

## v2.0.0 (2021-02-03)


## v0.1.1 (2021-02-03)
### Other
* Merge branch 'cdnjs-crawler' into 'master' ([`a34010a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/a34010ace84073d86bee35b60f93ef948876ee38))
* Add file crawling example  ([`748b915`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/748b9151e49659a10824d17516fb9ce01f3c47f7))
* :memo: Extend docstrings  ([`71c3ffc`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/71c3ffcb1d0bbf49dfabeb140d71988fd02f2213))
* :white_check_mark: Add actual test cases  ([`7d3fd25`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/7d3fd257b8499ba748427c6c85c56af6ef0b0cf0))
* :rotating_light: Fix linter error  ([`a860424`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/a860424f6f7838433cb6d9956778ac706b31722d))
* :tada: First basic implementation of cdnjs crawler  ([`035573a`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/035573ae9984535a1597d37e79615ba1d2bbab0e))
* :rotating_light: Fix CI errors due to incompatible options from copied boilerplate code  ([`05c967e`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/05c967ec20148cfaa37ff7d8e9c62ac01d9f82e4))
* Test CI/CD tools  ([`b4cff7e`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/b4cff7eb2ff85b8726ad96fdc925c82df8fc1549))
* Test CI/CD tools  ([`a601fdf`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/a601fdf35fa22f955d4e071ace0c0c7e6353ee65))
* Copy over CI/CD and code base  ([`e911afd`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/e911afd1f30851f67582f1775574bd345bce62f1))
* Init  ([`840dc92`](https://gitlab.com/weasel-project/weasel-data-sources/-/commit/840dc92a02431dd344ece3adfac973ae8089c252))

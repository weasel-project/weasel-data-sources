Examples
========

Crawling files
--------------

In this example we implement a simple crawler to retrieve and store all files of `jQuery` from cdnjs.com.

.. code-block:: python

    import os
    import shutil
    from weasel_data_sources.releases import CDNJSFetcher


    # Helper function to create potentially missing directories
    def _ensure_directory(file_path):
        directory = os.path.dirname(file_path)
       if not os.path.exists(directory):
           os.makedirs(directory)


    def crawl_library_files(fetcher, out_path):
        # Iterate over all known releases
        for release in fetcher.get_release_metadata():
            ver = release['version_str']

            # Iterate over all files of this release
            for path in fetcher.get_file_list(ver):
                # Build a target path to which we will store the retrieved file
                target = os.path.join(out_path, ver, path)
                _ensure_directory(target)

                # To retrieve a file we get a standard python `file` object
                with fetcher.retrieve_file(ver, path) as fp_source, open(target, 'wb') as fp_target:
                    shutil.copyfileobj(fp_source, fp_target)

    if __name__ == '__main__':
        fetcher = CDNJSFetcher('jquery')
        crawl_library_files(fetcher, 'output')

As can be seen, we create a :class:`~weasel_data_sources.releases.CDNJSFetcher` with the according library name as a parameter.
We only use methods which are defined in its base class, such that we could also replace the fetcher with a :class:`~weasel_data_sources.releases.GitFetcher`.


Using included technology sources
---------------------------------

In the previous example we created the fetcher instance on our own by supplying the right library name.
However, `weasel_data_sources` also ships with a collection of cdnjs library names, git repositories and CPE identifiers for several known technologies.
This allows us to create different fetchers more easily.
Using `crawl_library_files` from above, we implement a crawler for all included release sources.

.. code-block:: python

    import os
    from crawl_files import crawl_library_files
    from weasel_data_sources import TechnologyBuilder

    if __name__ == '__main__':
        builder = TechnologyBuilder('local_git_cache')

        for technology_name, technology_handler in builder.get_all_technologies().items():
            print(technology_name)
            if technology_handler.has_cdnjs:
                fetcher = technology_handler.create_cdnjs_fetcher()
                crawl_library_files(fetcher, os.path.join('output_cdnjs', technology_name))

            if technology_handler.has_git:
                fetcher = technology_handler.create_git_fetcher()
                crawl_library_files(fetcher, os.path.join('output_git', technology_name))


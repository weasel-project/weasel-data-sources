WEASEL Data Sources
===================

`weasel-data-sources` is a collection of data sources to retrieve information about software releases and vulnerabilities.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

        Basics <basics>
        Examples <examples>
        API Documentation <api>


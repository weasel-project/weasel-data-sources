Basics
======

`weasel_data_sources` includes different fetchers to retrieve information about releases and known vulnerabilities of different technologies.

Release fetchers
----------------

To get basic information about releases of a technology we use different kinds of release fetchers (see :class:`~weasel_data_sources.releases.ReleaseFetcher` ).
Their purpose is not only to retrieve available releases of a software and corresponding metadata,
but also to get information (such as names and hashes for identification) about included files and to copy them.
Currently there are two different fetchers implemented:
 *  The :class:`~weasel_data_sources.releases.CDNJSFetcher` covers JavaScript libraries available through `cdnjs <https://cdnjs.com/>`_.
    Besides available releases, this fetcher yields information on the most recent release and file hashes, before actually fetching the file from cdnjs.
    Please keep in mind that older releases might not be covered, i.e. if they were already outdated when a library was added to cdnjs.
 *  The :class:`~weasel_data_sources.releases.GitFetcher` clones and works on arbitrary Git repositories.
    Available releases are extracted from tagged commits in the repo.
    In comparison to the `CDNJSFetcher` this usually also older releases and beta versions of the software.
    Additionally, release information also include the publication date, based on the date of the tagged commit.

See also :doc:`Examples <examples>` to learn more about the usage of these fetchers.

Vulnerability fetchers
----------------------

To gather vulnerability information, there currently only is support for crawling from (arbitrary) `cve-search <https://github.com/cve-search/cve-search>`_ instances.
The fetcher (see :class:`~weasel_data_sources.vulnerabilities.VulnerabilityFetcher` ) is able to get known vulnerabilities by supplying the CPE base of a technology and the version string of interest.
Retrieved information of vulnerabilities contain the CVEs, CVSS scores and the vulnerability publication dates.

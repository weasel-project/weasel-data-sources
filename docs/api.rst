API Documentation
=================

TechnologyBuilder
-----------------
.. autoclass:: weasel_data_sources.TechnologyBuilder
	:members:
	:undoc-members:

TechnologyHandler
-----------------
.. autoclass:: weasel_data_sources.mapping.mapping.TechnologyHandler
	:members:
	:undoc-members:


ReleaseFetcher
--------------
.. autoclass:: weasel_data_sources.releases.ReleaseFetcher
	:members:
	:undoc-members:

.. autoclass:: weasel_data_sources.releases.CDNJSFetcher
	:special-members: __init__

.. autoclass:: weasel_data_sources.releases.GitFetcher
	:special-members: __init__

.. autoclass:: weasel_data_sources.releases.SVNFetcher
	:special-members: __init__


VulnerabilityFetcher
--------------------
.. autoclass:: weasel_data_sources.vulnerabilities.VulnerabilityFetcher
	:members:
	:undoc-members:

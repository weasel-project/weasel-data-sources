import os
import shutil
from weasel_data_sources.releases import CDNJSFetcher


def _ensure_directory(file_path):
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)


def crawl_library_files(fetcher, out_path):
    i = 0
    for release in fetcher.get_release_metadata():
        ver = release['version_str']
        for path in fetcher.get_file_list(ver):
            target = os.path.join(out_path, ver, path)
            _ensure_directory(target)

            print('Saving to {}'.format(target))
            with fetcher.retrieve_file(ver, path) as fp_source, open(target, 'wb') as fp_target:
                shutil.copyfileobj(fp_source, fp_target)

            i += 1

    print('Fetched {} files.'.format(i))


if __name__ == '__main__':
    fetcher = CDNJSFetcher('jquery')
    crawl_library_files(fetcher, 'output')

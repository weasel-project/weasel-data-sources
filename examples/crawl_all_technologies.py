import os
from crawl_files import crawl_library_files
from weasel_data_sources import TechnologyBuilder

if __name__ == '__main__':
    builder = TechnologyBuilder('local_git_cache')

    for technology_name, technology_handler in builder.get_all_technologies().items():
        print(technology_name)
        if technology_handler.has_cdnjs:
            fetcher = technology_handler.create_cdnjs_fetcher()
            crawl_library_files(fetcher, os.path.join('output', technology_name))

        if technology_handler.has_repository:
            fetcher = technology_handler.create_repository_fetcher()
            crawl_library_files(fetcher, os.path.join('output_repo', technology_name))

import pytest

from weasel_data_sources.releases import ReleaseFetcher


class TestBaseSource:
    def test_base_unimplemented(self):
        fetcher = ReleaseFetcher()

        with pytest.raises(NotImplementedError):
            fetcher.get_release_metadata()

        with pytest.raises(NotImplementedError):
            fetcher.get_file_list('1.1.0')

        with pytest.raises(NotImplementedError):
            fetcher.retrieve_file('1.1.0', 'foo.txt')


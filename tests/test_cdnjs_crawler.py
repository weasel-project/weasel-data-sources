import json
import os
import tempfile
import pytest
import shutil

from unittest import TestCase
from httmock import all_requests, urlmatch, HTTMock
from weasel_data_sources.releases import CDNJSFetcher


@pytest.fixture()
def cdnjs_fetcher():
    with open('tests/data/cdnjs_jquery.json') as data_file:
        data = json.load(data_file)

    @urlmatch(netloc='api.cdnjs.com', path='/libraries/jquery')
    def data_response(url, request):
        return json.dumps(data)

    with HTTMock(data_response):
        fetcher = CDNJSFetcher('jquery')

    return fetcher


class TestCDNJSCrawler:
    def test_release_metadata(self, cdnjs_fetcher):
        retrieved = cdnjs_fetcher.get_release_metadata()
        required = [
            {"version_str": "1.10.0", "latest": False, "published": True},
            {"version_str": "1.10.1", "latest": False, "published": True},
            {"version_str": "3.5.0", "latest": False, "published": True},
            {"version_str": "3.5.1", "latest": True, "published": True},
        ]

        TestCase().assertListEqual(retrieved, required)

    def test_file_list(self, cdnjs_fetcher):
        required = {
            "jquery.js": {"hashes": {"sha512": "58d2f17cfffc71560bf6c8fc267a7a7add0192e6cb3f7d638531bdbe12ff179b84666839c04ccaa17a75909b25ccf416c0f4f57b23224b194a0a0cc72ce4ce4d"}},
            "jquery.min.js": {"hashes": {"sha512": "6cb4f4426f559c06190df97229c05a436820d21498350ac9f118a5625758435171418a022ed523bae46e668f9f8ea871feab6aff58ad2740b67a30f196d65516"}},
            "jquery.slim.js": {"hashes": {"sha512": "d656a08cb7e70b52348aa1fda651d8214ab7bc331f8e166c2f2f5e95f2bcf5105ca5c45cc7897e9112414a71e1d8c87a90bc511e839b0f533951351b805cba9c"}},
            "jquery.slim.min.js": {"hashes": {"sha512": "fc35d35ebea742874c522abe2142580add8f3ce523ac727dc05aeaa49dd79203cd39955f32893b711c3a092c72090c579faa339444ac4a1d7fb0c093175acbfe"}},
        }
        TestCase().assertDictEqual(cdnjs_fetcher.get_file_list('3.5.1'), required)

    def test_retrieve_file(self, cdnjs_fetcher):
        with open('tests/data/foo.txt') as data_file:
            data = data_file.read()

        @urlmatch(netloc='cdnjs.cloudflare.com', path='/ajax/libs/jquery/3.5.1/jquery.min.js')
        def data_response(url, request):
            return data

        with HTTMock(data_response), tempfile.TemporaryDirectory() as tmp_dir:
            target_file_name = os.path.join(tmp_dir, 'test.min.js')

            with cdnjs_fetcher.retrieve_file('3.5.1', 'jquery.min.js') as fp_source, open(target_file_name, 'wb') as fp_target:
                shutil.copyfileobj(fp_source, fp_target)

            with open(target_file_name) as fp:
                assert data == fp.read()


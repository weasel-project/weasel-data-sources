import tarfile
import tempfile
import os
import pytest
import datetime
import pytz

from unittest import TestCase
from weasel_data_sources.releases import GitFetcher, SVNFetcher


@pytest.fixture(scope="class", params=[
    ('test_repo_git', GitFetcher, ''),
    ('test_repo_svn', SVNFetcher, 'file:///')
])
def repo_fetcher(request):
    repo_name = request.param[0]
    fetcher_class = request.param[1]
    protocol = request.param[2]

    with tempfile.TemporaryDirectory() as tmp_target:
        with tempfile.TemporaryDirectory() as tmp_source:
            with tarfile.open('tests/data/{}.tar.xz'.format(repo_name)) as tar:
                tar.extractall(tmp_source)

            fetcher = fetcher_class(protocol + os.path.join(tmp_source, repo_name), os.path.join(tmp_target, repo_name))
        yield fetcher


class TestRepositoryCrawler:
    def test_release_metadata(self, repo_fetcher):
        retrieved = repo_fetcher.get_release_metadata()
        required = [
            {"version_str": "1.0", "publication_date": datetime.datetime(2021, 2, 3, 12, 35, 52, tzinfo=pytz.UTC)},
            {"version_str": "1.0.1", "publication_date": datetime.datetime(2021, 2, 3, 12, 49, 50, tzinfo=pytz.UTC)}
        ]

        TestCase().assertListEqual(retrieved, required)

    def test_file_list(self, repo_fetcher):
        required = {
            "1.0": {
                "foo.txt": {
                    "hashes": {
                        "sha512": "9b75ddb74674b45ab738f84f73ef25c833d7d33d7c72d2556f13274d753259187386bf91dadf8e6a735e6111d703d3ffbabf64d827aaec64d5c6c33259260ce9"
                    }
                },
                "test": {
                    "hashes": {
                        "sha512": "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"
                    }
                },
            },
            "1.0.1": {
                "foo.txt": {
                    "hashes": {
                        "sha512": "9b75ddb74674b45ab738f84f73ef25c833d7d33d7c72d2556f13274d753259187386bf91dadf8e6a735e6111d703d3ffbabf64d827aaec64d5c6c33259260ce9"
                    }
                },
                "bar.txt": {
                    "hashes": {
                        "sha512": "f7fbba6e0636f890e56fbbf3283e524c6fa3204ae298382d624741d0dc6638326e282c41be5e4254d8820772c5518a2c5a8c0c7f7eda19594a7eb539453e1ed7"
                    }
                },
                "test": {
                    "hashes": {
                        "sha512": "f7fbba6e0636f890e56fbbf3283e524c6fa3204ae298382d624741d0dc6638326e282c41be5e4254d8820772c5518a2c5a8c0c7f7eda19594a7eb539453e1ed7"
                    }
                },
            }
        }

        for version, files in required.items():
            TestCase().assertDictEqual(repo_fetcher.get_file_list(version), files)

    def test_retrieve_file(self, repo_fetcher):
        with open('tests/data/foo.txt', 'rb') as orig, repo_fetcher.retrieve_file('1.0', 'foo.txt') as retrieved:
            assert orig.read() == retrieved.read()

        with open('tests/data/bar.txt', 'rb') as orig, repo_fetcher.retrieve_file('1.0.1', 'bar.txt') as retrieved:
            assert orig.read() == retrieved.read()

